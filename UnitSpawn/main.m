//
//  main.m
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/17/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Action.h"

int main(int argc, const char * argv[]) {
    
    @autoreleasepool {
        Action *battle = [[Action alloc] init];
        [battle execute];
    }
    return 0;
}
