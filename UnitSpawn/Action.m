//
//  Action.m
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/22/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "Action.h"

@implementation Action

// Main method
- (void)execute
{
    [self spawnUnits];
//    [self unitPresents];
    [self berserkerModeOn];
}

- (void)spawnUnits
{
    //        Spawn healers
    Healer *healerFirst = [[Healer alloc] init];
    [healerFirst setUnitX:9];
    [healerFirst setUnitY:7];
    [healerFirst setUnitId:1];
    
    Healer *healerSecond = [[Healer alloc] init];
    [healerSecond setUnitX:2];
    [healerSecond setUnitY:2];
    [healerSecond setUnitId:2];
    
    Healer *healerThird = [[Healer alloc] init];
    [healerThird setUnitX:3];
    [healerThird setUnitY:9];
    [healerThird setUnitId:6];
    
    //        Spawn warriors
    Warrior *warriorFirst = [[Warrior alloc] init];
    [warriorFirst setUnitX:4];
    [warriorFirst setUnitY:7];
    [warriorFirst setUnitId:3];
    
    Warrior *warriorSecond = [[Warrior alloc] init];
    [warriorSecond setUnitX:1];
    [warriorSecond setUnitY:8];
    [warriorSecond setUnitId:4];
    
    Warrior *warriorThird = [[Warrior alloc] init];
    [warriorThird setUnitX:6];
    [warriorThird setUnitY:9];
    [warriorThird setUnitId:5];
    
    Warrior *warriorFourth = [[Warrior alloc] init];
    [warriorFourth setUnitX:6];
    [warriorFourth setUnitY:3];
    [warriorFourth setUnitId:7];
    
//    NSArray* units = [NSArray arrayWithObjects: healerFirst, healerSecond, healerThird, warriorFirst, warriorSecond, warriorThird, warriorFourth, nil];
    NSArray* units = @[healerFirst, healerSecond, healerThird, warriorFirst, warriorSecond, warriorThird, warriorFourth];
    NSArray* healers = @[healerFirst, healerSecond, healerThird];
    NSArray* warriors = @[warriorFirst, warriorSecond, warriorThird, warriorFourth];

    [self setUnits:units];
    [self setHealers:healers];
    [self setWarriors:warriors];
}

// All units are represent themselves
- (void)unitPresents
{
    for (Unit *unitElement in [[self aliveUnits] copy])
    {
        [unitElement unitPresents];
    }
}

// Pick random warrior
- (void)pickRandomWarrior
{
    NSInteger unitsCount = [[self warriors] count];
//    NSInteger indx = arc4random_uniform(unitsCount);
    NSInteger indx = random() % unitsCount;                     // for some reason it picks the same unit everytime
    Unit *chosenUnit = [[self warriors] objectAtIndex:indx];    // for first warrior
    
    while ([chosenUnit isDead] != NO)
    {
        NSInteger unitsCount = [[self warriors] count];
        NSInteger indx = random() % unitsCount;
        
        chosenUnit = [[self warriors] objectAtIndex:indx];
    }
    
    NSLog(@"Attacker is Unit %li", [chosenUnit unitId]);
    [self setAttackingUnit:chosenUnit];
}

// Pick random healer
- (void)pickRandomHealer
{
    NSMutableArray *aliveHealers = [NSMutableArray new];
    
    for (Unit *currentUnit in [[self healers] copy])
    {
        if ([currentUnit isDead] == NO)
        {
            [aliveHealers addObject:currentUnit];
        }
    }
    
    if ([aliveHealers count] > 0)
    {
        NSInteger unitsCount = [aliveHealers count];
        NSInteger indx = random() % unitsCount;
        Unit *chosenUnit = [aliveHealers objectAtIndex:indx];
        
        NSLog(@"Healer is Unit %li", [chosenUnit unitId]);
        [self setHealingUnit:chosenUnit];
    }
}

// Find nearest unit
- (void)lookupForNearestUnit
{
    NSInteger chosenUnitX, chosenUnitY, currentUnitX, currentUnitY, distance = 0, minimumDistance;
    
    chosenUnitX = [[self attackingUnit] unitX];
    chosenUnitY = [[self attackingUnit] unitY];
    
    NSLog(@"Chosen unit has x=%li; y=%li", chosenUnitX, chosenUnitY);
    
    minimumDistance = NSIntegerMax;
    
    Unit *nearestUnit = nil;
    
    for (Unit *currentUnit in [self units])
    {
        currentUnitX = [currentUnit unitX];
        currentUnitY = [currentUnit unitY];
        
        distance = (pow(labs(chosenUnitX - currentUnitX), 2) + (pow(labs(chosenUnitY - currentUnitY), 2))); // calculating distance to unit
        
        if (nearestUnit != nil)                   // if nearest unit still wasn't considered
        {
            if ((distance < minimumDistance) && (distance > 0))
            {
                nearestUnit = currentUnit;        // pointer to nearest
                minimumDistance = distance;
            }
        }
        else if (distance > 0)
        {
            nearestUnit = currentUnit;
            minimumDistance = distance;
        }
    }
    
    NSLog(@"Nearest unit's ID %li and it located at (%li;%li)", [nearestUnit unitId], [nearestUnit unitX], [nearestUnit unitY]);
    [self setNearestUnit:nearestUnit];
}

// Calculate alive units
- (NSArray *)aliveUnits
{
    NSMutableArray *aliveUnits = [NSMutableArray new];
    
    for (Unit *currentUnit in [[self units] copy])
    {
        if ([currentUnit isDead] == NO)
        {
            [aliveUnits addObject:currentUnit];
        }
    }
    
    return [aliveUnits copy];
}

// Choose victim for battle
- (void)chooseVictim
{
    NSArray *units = [self aliveUnits];
    NSMutableArray *assumedVictims = [NSMutableArray new];
    
    for (Unit *currentUnit in units)
    {
        if (currentUnit != [self attackingUnit])
        {
            [assumedVictims addObject:currentUnit];
        }
    }
    
    NSInteger unitsCount = [assumedVictims count];
    
    NSInteger indx = random() % unitsCount;                     // for some reason it works incorrectly
//    NSInteger indx = arc4random_uniform(unitsCount);
    Unit *chosenVictim = [assumedVictims objectAtIndex:indx];

    if ([chosenVictim isDead] == NO)
    {
        [self setChosenVictim:chosenVictim];
    }
}

// The battle
- (void)berserkerModeOn
{
    NSInteger roundCount = 0;
    
    NSLog(@"<<<< FIGHT! >>>>");
    [self pickRandomWarrior];
    [self pickRandomHealer];

    while ([[self aliveUnits] count] > 1)
    {
        roundCount++;
        NSLog(@"==== Round %li ====", roundCount);
        [self unitPresents];
        [self chooseVictim];
        NSLog(@"~~~~~~~~~~~~~~~~~~~");
        [[self attackingUnit] damageUnit:[self chosenVictim]];
        
        if (roundCount % 2 == 0)                        // considering even round
        {
            [self healWeakestUnit];
        }
        NSLog(@"==== Round %li ended ====\n ", roundCount);
    }
    
    NSLog(@"Battle finished. Total rounds count: %li", roundCount);
}

// Considering the weakest unit
- (void)healWeakestUnit
{
    NSInteger maxDeltaHP, deltaHP;
    
    maxDeltaHP = 0;
    
    Unit *weakestUnit = nil;
    
    for (Unit *currentUnit in [self aliveUnits])
    {
        deltaHP = [currentUnit unitMaxHP] - [currentUnit unitHP];
        if (deltaHP > maxDeltaHP) {
            weakestUnit = currentUnit;
            maxDeltaHP = deltaHP;
        }
    }
    
    [[self healingUnit] healUnit:weakestUnit];
}

@end
