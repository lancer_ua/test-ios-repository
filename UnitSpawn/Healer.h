//
//  Healer.h
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/19/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "Unit.h"

@interface Healer : Unit

- (void)healUnit:(Unit *)uId;
- (void)unitDies;
- (NSInteger)unitMaxHP;
- (NSInteger)unitHeal;

@end
