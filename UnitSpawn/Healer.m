//
//  Healer.m
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/19/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "Healer.h"

@implementation Healer

// Healing
- (void)healUnit:(Unit *)uId
{
    if ([self isDead] == NO)
    {
        [uId receiveHP:[self unitHeal]];
    }
}

- (void)unitPresents
{
    NSLog(@"I'm healer #%li, I'm here (%li;%li), I have %li/%li HP and %li reduce rate. Whom could I heal?",
          [self unitId], [self unitX], [self unitY], [self unitHP], [self unitMaxHP], [self unitReduceDamage]);
}

- (void)unitDies
{
    NSLog(@"Unit %li:'I was a good healer.. AHHhhh..'", [self unitId]);
}

// Healer stats
- (NSInteger)unitMaxHP
{
    return 50;
}

- (NSInteger)unitHeal
{
    return 20;
}

- (NSInteger)unitReduceDamage
{
    return 0;
}

@end
