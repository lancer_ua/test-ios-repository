//
//  Unit.h
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/17/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Unit : NSObject

{
    NSInteger unitX, unitY, unitId;
    NSInteger unitHP, currentUnitHP, currentUnitMaxHP;
    BOOL isDead;
}

@property (nonatomic, readonly) NSInteger unitHP;
@property (nonatomic, readonly) BOOL isDead;
@property (nonatomic) NSInteger unitX;
@property (nonatomic) NSInteger unitY;
@property (nonatomic) NSInteger unitId;

- (void)unitPresents;
- (void)unitDies;
- (NSInteger)unitMaxHP;
- (NSInteger)unitHeal;
- (NSInteger)unitInjure;
- (NSInteger)unitReduceDamage;

@end

@interface Unit (ManageUnitHP)

- (void)receiveDamage:(NSInteger)uHP;
- (void)receiveHP:(NSInteger)uHP;
- (void)damageUnit:(Unit *)uId;
- (void)healUnit:(Unit *)uId;

@end