//
//  Unit+ManageUnitHP.h
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/26/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "Unit.h"

@interface Unit (ManageUnitHP)

@property (nonatomic, readwrite) NSInteger unitHP;

- (void)damageUnit:(NSInteger)uHP;
- (void)healUnit:(NSInteger)uHP;

@end