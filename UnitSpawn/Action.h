//
//  Action.h
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/22/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Healer.h"
#import "Warrior.h"

@interface Action : NSObject

@property (nonatomic, strong)NSArray* units;
@property (nonatomic, strong)NSArray* healers;
@property (nonatomic, strong)NSArray* warriors;
@property (nonatomic, weak)Unit* nearestUnit;
@property (nonatomic, weak)Unit* attackingUnit;
@property (nonatomic, weak)Unit* healingUnit;
@property (nonatomic, weak)Unit* chosenVictim;

- (void)lookupForNearestUnit;
- (void)execute;

@end
