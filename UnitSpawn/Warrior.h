//
//  Warrior.h
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/19/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "Unit.h"

@interface Warrior : Unit

- (void)damageUnit:(Unit *)uId;
- (void)unitDies;
- (NSInteger)unitMaxHP;
- (NSInteger)unitInjure;

@end
