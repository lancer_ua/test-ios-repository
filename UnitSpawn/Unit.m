//
//  Unit.m
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/17/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "Unit.h"

@interface Unit ()

@property (nonatomic, readwrite) NSInteger unitHP;
@property (nonatomic, readwrite) BOOL isDead;

@end

@implementation Unit

@synthesize unitHP, unitX, unitY, unitId, isDead;

- (id)init
{
    if (self = [super init])
    {
        [self setUnitHP:[self unitMaxHP]];
        [self setIsDead:NO];
    }
    return self;

}

// Unit tells about himself
- (void)unitPresents
{
}

// Unit reacts depending on his HP
- (void)unitDies
{
}

// Unit is performing action to chosen unit
- (void)performAction:(Unit *)uId
{
}

// Set unit HP when unit is injured
- (void)receiveDamage:(NSInteger)uHP
{
    NSInteger unitInjure;
    
    float reduceDamage = [self unitReduceDamage];
    unitInjure = uHP - (uHP * (reduceDamage/100));
    [self setUnitHP:([self unitHP] - unitInjure)];
    
    if ([self unitHP] <= 0)
    {
        NSLog(@"-%li HP to unit %li. Unit %li is dead.", unitInjure, [self unitId], [self unitId]);
        [self unitDies];
        [self setUnitHP:0];
        [self setIsDead:YES];
    }
    else if ([self unitHP] > 0)
    {
        NSLog(@"-%li HP to unit %li. Injured unit has %li HP.", unitInjure, [self unitId], [self unitHP]);
        
        if ([self unitHP] <= ([self unitMaxHP]/2))
        {
            NSLog(@"Unit %li: Ай бля!", [self unitId]);
        }
    }
}

// Set unit HP when unit is healed
- (void)receiveHP:(NSInteger)uHP
{
    NSInteger deltaHP;
    
    currentUnitHP = [self unitHP];
//    currentUnitMaxHP = [self unitMaxHP];
    deltaHP = [self unitMaxHP] - [self unitHP];
    
    if (currentUnitHP > 0)
    {
        if (deltaHP >= uHP)
        {
            [self setUnitHP:currentUnitHP + uHP];
        }
        else if (deltaHP < uHP)
        {
            uHP = deltaHP;
            [self setUnitHP:currentUnitHP + uHP];
        }
        
        NSLog(@"+%li HP to Unit %li. Now he has %li HP.", uHP, [self unitId], [self unitHP]);
    }

    if (currentUnitHP == 0)
    {
        NSLog(@"Unit %li is dead. No need to heal.", [self unitId]);
    }
}

- (NSInteger)unitMaxHP
{
    NSAssert(NO, @"MaxHP: Abstract class is used");
    return 0;
}

- (NSInteger)unitHeal
{
    NSAssert(NO, @"Heal: Abstract class is used");
    return 0;
}

- (NSInteger)unitInjure
{
    NSAssert(NO, @"Injure: Abstract class is used");
    return 0;
}

- (NSInteger)unitReduceDamage
{
    NSAssert(NO, @"Reduce: Abstract class is used");
    return 0;
}

@end
