//
//  Warrior.m
//  UnitSpawn
//
//  Created by Pavel Smirnov on 2/19/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "Warrior.h"

@implementation Warrior

// Damage method
- (void)damageUnit:(Unit *)uId
{
    if ([self isDead] == NO)
    {
        if ([uId isDead] == NO)
        {
            [uId receiveDamage:[self unitInjure]];
        }
    }
}

- (void)unitPresents
{
    NSLog(@"I'm warrior #%li, I'm here (%li;%li), I have %li/%li HP and %li reduce rate. Whom could I kill?",
          [self unitId], [self unitX], [self unitY], [self unitHP], [self unitMaxHP], [self unitReduceDamage]);
}

- (void)unitDies
{
    NSLog(@"Unit %li:'I was the best warrior.. AHHhhh..'", [self unitId]);
}

// Warrior stats
- (NSInteger)unitMaxHP
{
    return 150;
}

- (NSInteger)unitInjure
{
    return 30;
}

- (NSInteger)unitReduceDamage
{
    return 30;
}

@end
